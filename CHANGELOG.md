## [1.8.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.7.2...v1.8.0) (2022-09-03)


### Features

* enable sonar rules ([72ba66f](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/72ba66f72faa14af619bb74ca35fda5698121128))

## [1.7.2](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.7.1...v1.7.2) (2022-09-03)


### Bug Fixes

* remove conflicting indent rule ([77193e7](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/77193e75043223e61ca70a49c5b80bb4f7bfeb58))

## [1.7.1](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.7.0...v1.7.1) (2022-09-03)


### Bug Fixes

* update dependencies and fix prettier ([d0176c2](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/d0176c27979137dfb07b3c7fc98bb6ab8ee785eb))

## [1.7.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.6.0...v1.7.0) (2022-09-03)


### Features

* add prettier ([d369e4d](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/d369e4d1f88fcda9d1b48899e71283305d35a15f))

# [1.6.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.5.0...v1.6.0) (2022-08-26)


### Features

* fix npm config ([77ffddd](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/77ffddd5a8b4822f30332b861d5f0efeecc05d06))

# [1.5.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.4.0...v1.5.0) (2022-08-26)


### Features

* fix npm config ([5f3876f](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/5f3876f02d9c8dd90ee6577bce9bef29adb76721))

# [1.4.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.3.0...v1.4.0) (2022-08-26)


### Features

* migrate to consolidate release config ([6afdf9c](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/6afdf9c1b952ded984bac0b880c30d442fc63b90))
* use semantic-release-config ([d1a25fe](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/d1a25fea9e4158855fc7ebf5234206358c470941))

# [1.3.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.2.0...v1.3.0) (2022-08-26)


### Features

* include eslint and update readme ([9891c3a](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/9891c3a7eb52beb6e7b139f35c77a0e304898878))

# [1.2.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.3...v1.2.0) (2022-08-26)


### Features

* fix plugin issue ([5006524](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/50065249170fb42eb53c5d2b1e64ac5d9077bcf5))

## [1.1.3](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.2...v1.1.3) (2022-08-26)


### Bug Fixes

* npm publish with ci ([ea68cf8](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/ea68cf808dc122eb2b77759f00e6cd33c71ccc77))

## [1.1.2](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.1...v1.1.2) (2022-08-26)


### Bug Fixes

* npm publish with ci ([2ae3541](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/2ae35415d021e828bd88b68953a4f64dbaa8dbeb))

## [1.1.1](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.0...v1.1.1) (2022-08-26)


### Bug Fixes

* npm publish with ci ([1ddca00](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/1ddca002b91f29dfd1a651d18021ce95f70f28a6))

# [1.1.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.0.0...v1.1.0) (2022-08-26)


### Bug Fixes

* node image ([ed6bfeb](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/ed6bfeb018abe5b9a949ca1fa3e78034e55b60fc))


### Features

* add publish step ([be8a985](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/be8a98535c327ede190f33196fb68971867adfe5))
* add publish step ([93f4c21](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/93f4c21a203db52d73a8ea219f2a16157773135d))
* fix npm release ([b7e3b46](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/b7e3b46d50f93e40dd604474e6199a9f22da979f))

# 1.0.0 (2022-08-26)


### Features

* add commitlint ([75bf411](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/75bf411742f1a3aa2149b2f47f214ef6fa1fc899))
* initial commit ([18b9b77](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/18b9b77492016c27d930ede21d2cb4e611b6d352))
