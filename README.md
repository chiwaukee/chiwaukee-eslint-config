# @chiwaukee/eslint-config

## Usage

```shell
npm i --save-dev @chiwaukee/eslint-config
```

in `.eslintrc.js`:

```js
module.exports = {
    extends: '@chiwaukee'
}
```

Add a `lint` script to your `package.json` that looks like:

```shell
"lint": "eslint --fix 'src/**/*.{js,ts,tsx}'",
```

That command will fix auto-fixable issues on JS and typescript files, and error out if there are any issues.