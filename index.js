module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true
    },
    extends: ['plugin:react/recommended', 'standard', 'plugin:react/jsx-runtime', 'plugin:prettier/recommended', "plugin:sonarjs/recommended"],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        ecmaVersion: 'latest',
        sourceType: 'module'
    },
    plugins: ['react', '@typescript-eslint', 'sonarjs', 'prettier'],
    rules: {
        'no-multiple-empty-lines': 'error',
        "prettier/prettier": 'error'
    },
    settings: {
        react: {
            version: 'detect'
        }
    }
}
